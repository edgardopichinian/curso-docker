import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {DepartmentDetailsComponent} from './components/department-details/department-details.component';
import {AddDepartmentComponent} from './components/add-department/add-department.component';
import {DepartmentListComponent} from './components/department-list/department-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'departments', pathMatch: 'full' },
  { path: 'departments', component: DepartmentListComponent },
  { path: 'add/:id', component: AddDepartmentComponent },
  { path: 'add', component: AddDepartmentComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
