# Unidad 2 - Proyecto FrontEnd


# 2 Frontend Angular

# 2.1 Entorno de Desarrollo para Java con Docker

Podemos ejecutar el siguiente comando para vincular un contenedor Node con nuestro proyecto
Esto permite realizar operaciones con node sin la necesidad de tenerlo instalado en nuestra maquina.

```
docker run --rm -it --name node-docker -v ${pwd}:/home/app -w /home/app -e "PORT=3000" -p 8080:3000 -u node node:12.8.1 /bin/bash
```

### 2.1.1  Crear una Imagen y un Compose de Desarrollo

Con el siguiente **Dockerfile** creamos una imagen
```
FROM node:12.8.1

WORKDIR /home/app
ENV PORT 3000

RUN npm install -g @angular/cli

EXPOSE 3000

ENTRYPOINT /bin/bash
```

Con el siguiente commando creamos la imagen para node
```
docker build -t node-docker . -f nombre-de-dockerfile 
```

Luego con el siguiente comando levantamos un contenedor
```
docker run --rm -it --name node-docker -v ${PWD}:/home/app -p 8080:3000 node-docker
```

**docker-compose** con el servicio de base de datos necesario para que pueda levantar el servicio
```
version: "3"
services:
  nod_dev_env:
    build:
      context: .
      dockerfile: DockerfileNode
    container_name: node-docker
    ports:
      - "8080:3000"
    volumes:
      - ./:/home/app

```

```
docker-compose -f docker-compose-node.yml run --rm -p 4000:3000 nod_dev_env
```
  
Con el siguiente comando podemos obtener el **ID del contenedor**

```
docker ps -qf "name=frontend_nod_dev_env" 
```

Ahora ya podemos ingresar al contenedor Java11 con bash o directamente ejecutar las tareas de gradlew (build/clear/bootrun)
```
docker exec -it $(docker ps -qf "name=frontend_nod_dev_env") /bin/bash
docker exec -it $(docker ps -qf "name=frontend_nod_dev_env") npm install
```

## 2.2 Buscar la mejor imagen  para Angular 
```
FROM nginx:1.17.9-alpine
```

## 2.2.1 Armamos el Docker File 
```
FROM nginx:1.17.9-alpine
LABEL maintainer="olinares@unrn.edu.ar"
LABEL "unrn.edu.ar"="UNRN"
LABEL "service"="Angular Frontend"
COPY dist/frontend /usr/share/nginx/html
COPY frontend.conf /etc/nginx/nginx.conf

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]

```

## 2.2.1.2 Crear Imagen de Docker
```
docker build -t fronend:1.0 .
```

## 2.2.2 Armamos el Docker File Multistage
```
FROM node:lts-alpine as build-stage
WORKDIR /app
COPY *.json ./
RUN npm install
COPY . .
RUN npm run prod

FROM nginx:1.17.9-alpine as production-stage
ENV API_HOST localhost
ENV API_PORT 8080
COPY frontend.conf /etc/nginx/nginx.conf
COPY --from=build-stage /app/dist/frontend /usr/share/nginx/html

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]

```

## 2.2.2.2 Crear Imagen del multistage
```
docker build -t fronend:1.0 . -f DockerfileMS
```

## 2.2.3 Registry Local -  build y push
```
docker build -t 127.0.0.1:5000/fronend:1.0 .
docker push 127.0.0.1:5000/fronend:1.0
```

## 2.2.4 Crear contenedor de Docker y que corra en el puerto 9011
ERROR:  host not found in upstream "curso-backend:8080"
```
docker run --name curso-frontend -p 9011:80 frontend:1.0
```

## 2.2.5 Obtener el nombre de la red del backend
para poder linkear contenedores, es necesario que estos se encuentren en la misma
red, para poder hacerlo primero debemos obtener el nombre de la red.

Con el siguiente comando podemos listar todas las redes. 
```
docker network ls

docker inspect curso-backend --format='{{ .HostConfig.NetworkMode }}'

```
generalmente el nombre de la red es el nombre del contenedor mas _default

## 2.2.6 Agregar link --link curso-backend:curso-backend
```
docker run --name curso-frontend -p 9011:80 --link curso-backend:curso-backend --net backend_default  frontend:1.0
```
