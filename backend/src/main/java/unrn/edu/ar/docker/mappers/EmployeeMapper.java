package unrn.edu.ar.docker.mappers;

import org.mapstruct.*;
import unrn.edu.ar.docker.dto.DepartmentDTO;
import unrn.edu.ar.docker.dto.EmployeeDTO;
import unrn.edu.ar.docker.model.Department;
import unrn.edu.ar.docker.model.Employee;

import java.util.ArrayList;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface EmployeeMapper {

    @Mapping(target = "department", ignore = true)
    EmployeeDTO toDTO(Employee entity);

    @Mapping(target = "department", ignore = true)
    EmployeeDTO toDTOList(Employee entity, boolean ignore);

    @Mapping(target = "departmentId", ignore = true)
    Employee toEntity(EmployeeDTO dto);

    void mapToEntity(EmployeeDTO dto, @MappingTarget Employee entity);

    @AfterMapping
    default void setDepartment(@MappingTarget EmployeeDTO employeeDTO, Employee employee) {
        if (employee.getDepartmentId() != null)
            employeeDTO.setDepartment(new DepartmentDTO(employee.getDepartmentId()));

    }

    @AfterMapping
    default void setDepartment(@MappingTarget Employee employee, EmployeeDTO employeeDTO) {
        if (employeeDTO.getDepartment() != null)
            employee.setDepartmentId(employeeDTO.getDepartment().getId());
    }
}
